'use strict'
const path = require ('path');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require ('morgan');
const app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

const homeRoutes = require ('./routes/home');
const usersRoutes = require('./routes/users');

global.__basedir = __dirname;
app.use('/', express.static(__basedir + '/public'));
app.set ('view engine', 'ejs');
app.set ('views', path.join (__basedir, "/public/views/"));

app.use(morgan('dev'));

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,Authorization, Content-type,Accept,X-Access-Token,X-Key');
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

app.use('/users', usersRoutes);
app.use ('/', homeRoutes);

app.use(function(req, res, next) {
    const error = new Error ('Not Found');
    error.status = 404;
    next(error);
});

app.use (function(error, req, res, next) {
    res.status(error.status || 500);
    res.json ({
        error: {message: error.message}
    });
});

module.exports = app;
