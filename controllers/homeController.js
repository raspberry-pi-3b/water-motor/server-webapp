const urls = require ('../config/urls');
const security = require ('../tools/security');

exports.goToHome = async function (req, res, next) {
    let token, username;
    if (req.query.token) {
        try {
            let userData = security.authenticate (req.query.token);
            token = req.query.token;
            username = userData.username;
        } catch (e) {
            return res.status (400).redirect (urls.api.goToLogin);
        }
    }
    // ADD LOGIC TO SEND COMMAND TO NODE MCU AND GET STATUS OF WATER TANKS AND PASS IT TO THE RENDER METHOD AT THE END
    let data = {
        roof: 50,
        ground: 75
    };
    return res.render (urls.page.home, {token: token, username: username, data: data});
}

exports.goToMotorControl = async function (req, res, next) {
    let token, username, motorStatus;
    if (req.query.token) {
        try {
            let userData = security.authenticate (req.query.token);
            token = req.query.token;
            username = userData.username;
            try {
                let role = security.getRole (token);
                if (role === "Admin") {
                    motorStatus = true;
                    // ADD LOGIC TO SEND COMMAND TO NODE MCU AND GET STATUS OF MOTOR
                    return res.render (urls.page.motor, {token: token, username: username, motorStatus: motorStatus});
                }
                return res.status (400).redirect (urls.api.goToHome);
            } catch (e) {
                return res.status (400).redirect (urls.api.goToLogin);
            }
        } catch (e) {
            return res.status (400).redirect (urls.api.goToLogin);
        }
    } else return res.status (400).redirect (urls.api.goToLogin);
}

exports.toggleMotor = async function (req, res, next) {
    let token = req.body.token;
    console.log (token);
    // ADD LOGIC TO SEND COMMAND TO NODE MCU AND CHANGE STATUS OF MOTOR
    return res.redirect (urls.api.goToMotorControl + "?token=" + token);
}