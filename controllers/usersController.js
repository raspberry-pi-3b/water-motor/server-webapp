const utils = require('../tools/utils');
const urls = require ('../config/urls');


// Get user information from front end, call the userSC module to generate a new Ethereum address for the user and add all the information to the database
exports.addNewUser = async function (req, res, next) {
    if (req.body.name.length === 0) return res.status (400).redirect (urls.api.goToSignup + "?message=Name cannot be empty");
    if (req.body.phone.length === 0) return res.status (400).redirect (urls.api.goToSignup + "?message=Phone cannot be empty");
    if (req.body.password.length === 0) return res.status (400).redirect (urls.api.goToSignup + "?message=Password cannot be empty");
    if (req.body.retypepassword.length === 0) return res.status (400).redirect (urls.api.goToSignup + "?message=Retype password cannot be empty");
    var allAccounts = utils.getAccounts ();
    var username = req.body.name;
    var phone = req.body.phone;
    var password = req.body.password;
    var retypepassword = req.body.retypepassword;
    if (allAccounts [username]) return res.status (400).redirect (urls.api.goToSignup + "?message=Account already exists");
    if (retypepassword !== password) return res.status (400).redirect (urls.api.goToSignup + "?message=Password does not match");
    try {
        let output = utils.addAccount (username, phone, password);
        if (output) return res.redirect (urls.api.goToLogin);
    } catch (e) {
        res.status (400).send (e.message);
    } 
}

// If we are allowing the user to login using their email OR password, can we still just use email and password for hashing
exports.loginUser = async function (req, res, next) {
    var username = req.body.name;
    var password = req.body.password;
    var secString = utils.hashString (password);
    var allAccounts = utils.getAccounts ();
    if (allAccounts [username]) {
        if (allAccounts [username].password === secString) {
            var temp = {};
            temp = allAccounts [username];
            temp.username = username;
            res.locals.userData = temp;
            return next();
        }
        return res.status (400).redirect (urls.api.goToLogin + "?message=Incorrect username or password");
    } else {
        return res.status (400).redirect (urls.api.goToLogin + "?message=Account does not exist");
    }

}

exports.goToLoginPage = async function (req, res, next) {
    res.render (urls.page.login, {token: req.query.token, message: req.query.message});
}

exports.goToSignupPage = async function (req, res, next) {
    res.render (urls.page.signup, {token: req.query.token, message: req.query.message});
}