const express = require('express');
const router = express.Router();
const homeController = require ('../controllers/homeController');

router.get ('/home', homeController.goToHome);
router.get ('/motorcontrol', homeController.goToMotorControl);

router.post ('/togglemotor', homeController.toggleMotor);

module.exports = router;