const express = require('express');
const router = express.Router();
const security = require ('../tools/security');
const usersController = require ('../controllers/usersController');

// API to sign up a new user
router.post ('/addNewUser', usersController.addNewUser);
router.post ('/loginUser', usersController.loginUser, security.issueToken);
router.get ('/loginpage', usersController.goToLoginPage);
router.get ('/signuppage', usersController.goToSignupPage);

module.exports = router;