const jwt = require ('jsonwebtoken');
const config = require ('../config/config');
const urls = require ('../config/urls');
const utils = require ('./utils');

exports.authenticate = function (tok) {
    try {
        const decoded = jwt.verify (tok, config.jwt.secret);
        return decoded;
    } catch (e) {
        throw e;
    }
}

exports.issueToken = function (req, res, next) {
    var userData = res.locals.userData;
    var options = {expiresIn: config.jwt.expiresInMS} ;
    const token = jwt.sign ({username: userData.username, phone: userData.phone}, config.jwt.secret, options);
    return res.status (200).redirect (urls.api.goToHome + "?token=" + token);    
}

exports.getRole = function (tok) {
    try {
        const decoded = jwt.verify (tok, config.jwt.secret);
        let allAccounts = utils.getAccounts ();
        return allAccounts[decoded.username].role;
    } catch (e) {
        throw e;
    }
}


