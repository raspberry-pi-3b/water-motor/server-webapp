const crypto = require ('crypto');
const fs = require ('fs');
const path = require ('path');
const config = require ('../config/config');

Utils = {
    // Make first letter of each word capital
    // https://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript/196991#196991
    toTitleCase: function (str) {
        return str.replace (/\w\S*/g, function (txt) {
            return txt.charAt (0).toUpperCase () + txt.substr (1).toLowerCase ();
        });
    },

    toLower: function (str) {
        return str.toLowerCase();
    },

    hashString: function (str) {
        return crypto.createHash (config.sec.hash.mode).update (str).digest (config.sec.hash.base);
    },

    encryptString: function (str) {
        var myKey = crypto.createCipher (config.sec.crypt.mode, config.sec.crypt.secret);
        var cipherText = myKey.update (str, config.sec.crypt.encode, config.sec.crypt.base);
        cipherText += myKey.update.final (config.sec.crypt.base);
        return cipherText;
    },

    decryptString: function (str) {
        var myKey = crypto.createDecipher (config.sec.crypt.mode, config.sec.crypt.secret);
        var plainText = myKey.update (str, config.sec.crypt.base, config.sec.crypt.encode);
        plainText += myKey.update.final (config.sec.crypt.encode);
        return plainText;
    },

    hextToString: function (hex){
        var string = '';
        for (var i = 0; i < hex.length; i += 2) string += String.fromCharCode (parseInt (hex.substr (i, 2), 16));
        return string;
    },

    objectIdToInt: function (ObjectId) {
        return parseInt (ObjectId.toString (), 16);
    },

    httpsGet: async function (url, callback) {
        https.get (url, (res) => {
            let data ='';
            res.on ('data', (chunk) => data += chunk);
            res.on ('end', () => {
                callback ("SUCCESS", data);
            })
        }).on ('error', (e) => {
            callback ("ERROR", e);
        }).end();
    },

    writeToLog (text) {
        var logTime = new Date();
        var logString = '[' + logTime.getFullYear () + '-' + (logTime.getMonth () + 1) + '-' + logTime.getDate () + 'T' + logTime.getHours () + ':' + logTime.getMinutes () + ':' + logTime.getSeconds () + '.' + logTime.getMilliseconds () + ']\t';
        logString += text;
        fs.writeFileSync (path.join (__basedir, "logs.txt"), logString, function (err) {
            if (err) return console.log (err);
            console.log (text + "\tadded to log");
        });
    },

    base64Decode (fileName, data) {
        fs.writeFileSync( path.join (__basedir, config.storage.imagePath, (fileName + '.png')), data, 'base64');
    },

    addAccount (username, phone, password) {
        try {
            let raw = fs.readFileSync (path.join (__basedir, config.storage.accounts));        
            let accounts = JSON.parse(raw);        
            let hPassword = this.hashString (password);
            let user = {
                phone: phone,
                password: hPassword,
                role: this.toTitleCase ("user"),
                createdOn: new Date ()
            };
            accounts [username] = user;
            accounts = JSON.stringify (accounts, null, 4);
            try {
                fs.writeFileSync (path.join (__basedir, config.storage.accounts), accounts);
                return true;
            } catch (e) {
                throw e;
            }
        } catch (e) {
            throw e;
        }
    },

    getAccounts () {
        try {
            let raw = fs.readFileSync (__basedir + config.storage.accounts);
            let accounts = JSON.parse(raw);
            return accounts;
        } catch (e) {
            throw e;
        }
    }
};
module.exports = Utils;